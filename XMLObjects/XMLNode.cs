//#define NoEvents
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLObjects
{
#if !NoEvents
	public delegate void StringDelegate(EventArgs<string> e);
	public delegate void BoolDelegate(EventArgs<bool> e);
	public delegate void SubnodesDelegate(EventArgs<List<XMLNode>> e);
	public delegate void XMLNodeDelegate(EventArgs<XMLNode> e);
	public delegate void DictionaryDelegate(EventArgs<Dictionary<string, string>> e);
	public delegate void XMLAttributeDelegate(EventArgs<string, string> e);
#endif

	public class XMLNode
	{
#if !NoEvents
		public static event StringDelegate HeaderAccess;
		public static event BoolDelegate HasParentAccess;
		public static event BoolDelegate HasValueAccess;
		public static event BoolDelegate HasSubNodesAccess;
		public static event XMLAttributeDelegate XMLAttributeAccess;
		public static event BoolDelegate HasAttrAccess;
		public static event SubnodesDelegate SubNodesAccess;
		public static event StringDelegate ValueAccess;
		public static event StringDelegate NameAccess;
		public static event DictionaryDelegate AttributesAccess;
		public static event XMLNodeDelegate ParentAccess;
#endif

		protected List<XMLNode> subNodes = new List<XMLNode>();
		protected Dictionary<string, string> attributes = new Dictionary<string, string>();
		protected string name;
		protected string value;
		protected XMLNode parent;

		public string Value
		{
			get
			{
#if !NoEvents
				if(ValueAccess != null)
				{
					EventArgs<string> e = new EventArgs<string>(this, this.value, AccessType.GET);
					ValueAccess(e);
					return e.Value;
				}
#endif
				return this.value;
			}
			set
			{
#if !NoEvents
				if(ValueAccess != null)
				{
					EventArgs<string> e = new EventArgs<string>(this, value, AccessType.SET);
					ValueAccess(e);
					if(e.canceled)
						return;
					this.value = e.Value;
					return;
				}
#endif
				this.value = value;
			}
		}
		public string Name
		{
			get
			{
#if !NoEvents
				if(NameAccess != null)
				{
					EventArgs<string> e = new EventArgs<string>(this, this.name, AccessType.GET);
					NameAccess(e);
					return e.Value;
				}
#endif
				return this.name;
			}
			set
			{
#if !NoEvents
				if(NameAccess != null)
				{
					EventArgs<string> e = new EventArgs<string>(this, value, AccessType.SET);
					NameAccess(e);
					if(e.canceled)
						return;
					this.name = e.Value;
					return;
				}
#endif
				this.name = value;
			}
		}
		public Dictionary<string, string> Attributes
		{
			get
			{
#if !NoEvents
				if(AttributesAccess != null)
				{
					EventArgs<Dictionary<string, string>> e = new EventArgs<Dictionary<string, string>>(this, this.attributes, AccessType.GET);
					AttributesAccess(e);
					return e.Value;
				}
#endif
				return this.attributes;
			}
			set
			{
#if !NoEvents
				if(AttributesAccess != null)
				{
					EventArgs<Dictionary<string, string>> e = new EventArgs<Dictionary<string, string>>(this, value, AccessType.SET);
					AttributesAccess(e);
					if(e.canceled)
						return;
					this.attributes = e.Value;
					return;
				}
#endif
				this.attributes = value;
			}
		}
		public List<XMLNode> SubNodes
		{
			get
			{
#if !NoEvents
				if(SubNodesAccess != null)
				{
					EventArgs<List<XMLNode>> e = new EventArgs<List<XMLNode>>(this, this.subNodes, AccessType.GET);
					SubNodesAccess(e);
					return e.Value;
				}
#endif
				return this.subNodes;
			}
			set
			{
#if !NoEvents
				if(SubNodesAccess != null)
				{
					EventArgs<List<XMLNode>> e = new EventArgs<List<XMLNode>>(this, value, AccessType.SET);
					SubNodesAccess(e);
					if(e.canceled)
						return;
					this.subNodes = e.Value;
					return;
				}
#endif
				this.subNodes = value;
			}
		}
		public XMLNode Parent
		{
			get
			{
#if !NoEvents
				if(ParentAccess != null)
				{
					EventArgs<XMLNode> e = new EventArgs<XMLNode>(this, this.parent, AccessType.GET);
					ParentAccess(e);
					return e.Value;
				}
#endif
				return this.parent;
			}
			set
			{
#if !NoEvents
				if(ParentAccess != null)
				{
					EventArgs<XMLNode> e = new EventArgs<XMLNode>(this, value, AccessType.SET);
					ParentAccess(e);
					if(e.canceled)
						return;
					this.parent = e.Value;
					return;
				}
#endif
				this.parent = value;
			}
		}
		public string Header
		{
			get
			{
				string header = GetXMLHeader();
#if !NoEvents
				if(HeaderAccess != null)
				{
					EventArgs<string> e = new EventArgs<string>(this, header, AccessType.GET);
					HeaderAccess(e);
					return e.Value;
				}
#endif
				return header;
			}
		}
		public bool HasParent
		{
			get
			{
#if !NoEvents
				if(HasParentAccess != null)
				{
					EventArgs<bool> e = new EventArgs<bool>(this, this.parent != null, AccessType.GET);
					HasParentAccess(e);
					return e.Value;
				}
#endif
				return this.parent != null;
			}
		}
		public bool HasValue
		{
			get
			{
#if !NoEvents
				if(HasValueAccess != null)
				{
					EventArgs<bool> e = new EventArgs<bool>(this, this.value != null, AccessType.GET);
					HasValueAccess(e);
					return e.Value;
				}
#endif
				return this.value != null;
			}
		}
		public bool HasSubNodes
		{
			get
			{
				List<XMLNode> subNodes = this.SubNodes;
				bool hasSubNodes = subNodes != null ? subNodes.Count != 0 : false;
#if !NoEvents
				if(HasSubNodesAccess != null)
				{
					EventArgs<bool> e = new EventArgs<bool>(this, hasSubNodes, AccessType.GET);
					HasSubNodesAccess(e);
					return e.Value;
				}
#endif
				return hasSubNodes;
			}
		}
		public bool IsEmpty
		{
			get
			{
				return this.HasValue && !this.HasSubNodes;
			}
		}

		public string this[string key]
		{
			get
			{
				string result = this.attributes.ContainsKey(key) ? this.attributes[key] : null;
#if !NoEvents
				if(XMLAttributeAccess != null)
				{
					EventArgs<string, string> e = new EventArgs<string, string>(this, result, key, AccessType.GET);
					XMLAttributeAccess(e);
					return e.Value;
				}
#endif
				return result;
			}
			set
			{
				string val = value;
#if !NoEvents
				if(XMLAttributeAccess != null)
				{
					EventArgs<string, string> e = new EventArgs<string, string>(this, value, key, AccessType.SET);
					XMLAttributeAccess(e);
					if(e.canceled)
						return;
					val = e.Value;
				}
#endif
				if(this.attributes.ContainsKey(key))
					this.attributes[key] = val;
				else
					this.attributes.Add(key, val);
			}
		}


		public bool HasAttr(string attr)
		{
			bool result = this.attributes.ContainsKey(attr) && this.attributes[attr] != "";
#if !NoEvents
			if(HasAttrAccess != null)
			{
				EventArgs<bool> e = new EventArgs<bool>(this, result, AccessType.GET);
				HasAttrAccess(e);
				return e.Value;
			}
#endif
			return result;
		}


		public IEnumerable<XMLNode> GetSubNodes(string name)
		{
			return this.subNodes.Where((node) => node.name == name);
		}

		public XMLNode GetSubNode(string name)
		{
			try
			{
				return this.subNodes.First((node) => node.name == name);
			}
			catch (InvalidOperationException)
			{
				return null;
			}
		}

		public int GetSubNodesCount(string name)
		{
			return GetSubNodes(name).Count();
		}

		public bool HasSubNode(string name)
		{
			return GetSubNodesCount(name) != 0;
		}

		public bool HasSubNode(params string[] names)
		{
			foreach (string name in names)
				if (!HasSubNode(name))
					return false;
			return true;
		}

		public string GetXMLHeader(XmlWriter writer = null, bool fancy = true, Encoding e = null)
		{
			try
			{
				XmlWriter xtw;
				MemoryStream ms = null;
				if(writer == null)
				{
					XmlWriterSettings settings = new XmlWriterSettings();
					settings.Encoding = e = e ?? Encoding.UTF8;
					settings.OmitXmlDeclaration = true;
					if(fancy)
					{
						settings.Indent = true;
						settings.IndentChars = "\t";
					}
					else
						settings.Indent = false;
					xtw = XmlWriter.Create(ms = new MemoryStream(), settings);
				}
				else
					xtw = writer;

				xtw.WriteStartDocument();
				xtw.WriteStartElement(this.name);
				foreach(KeyValuePair<string, string> attr in this.attributes)
					xtw.WriteAttributeString(attr.Key, attr.Value);
				xtw.Flush();
				return e.GetString(ms.ToArray()) + ">";
			}
			catch
			{
				return null;
			}
		}


		public override string ToString()
		{
			return ToString(null);
		}

		public string ToString(XmlWriter writer = null, bool fancy = true, Encoding e = null)
		{
			XmlWriter xtw;
			MemoryStream ms = null;
			if(writer == null)
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Encoding = e = e ?? Encoding.UTF8;
				if(fancy)
				{
					settings.Indent = true;
					settings.IndentChars = "\t";
				}
				else
					settings.Indent = false;
				xtw = XmlWriter.Create(ms = new MemoryStream(), settings);
				xtw.WriteStartDocument();
			}
			else
				xtw = writer;

			xtw.WriteStartElement(this.name);
			foreach(KeyValuePair<string, string> attr in this.attributes)
				xtw.WriteAttributeString(attr.Key, attr.Value);
			if(this.HasValue)
				xtw.WriteString(this.value);
			else if(this.HasSubNodes)
				foreach(XMLNode node in this.subNodes)
					node.ToString(xtw);
			xtw.WriteEndElement();
			if(ms == null)
				return null;
			xtw.Close();
			return e.GetString(ms.ToArray());
		}

		public static XMLNode Create(Stream s)
		{
			return Create(new XmlTextReader(s));
		}
		public static XMLNode Create(string xml)
		{
			return Create(new XmlTextReader(new StringReader(xml)));
		}
		public static XMLNode CreateByPath(string path)
		{
			FileStream fs = null;
			try
			{
				return Create(fs = File.OpenRead(path));
			}
			finally
			{
				fs?.Close();
			}
		}
		public static XMLNode Create(XmlTextReader xtr)
		{
			XMLNode node = null;
			XMLNode rootNode = null;
			while (xtr.Read())
			{
				switch (xtr.NodeType)
				{
					case XmlNodeType.Element:
						node = new XMLNode() { name = xtr.Name, parent = node };
						if (node.parent != null)
							node.parent.subNodes.Add(node);
						while (xtr.MoveToNextAttribute())
							node.attributes.Add(xtr.Name, xtr.Value);
						xtr.MoveToElement();
						if(xtr.IsEmptyElement)
							node = node.parent;
						break;
					case XmlNodeType.Text:
						node.value = xtr.Value;
						break;
					case XmlNodeType.EndElement:
						if (node.parent == null)
							rootNode = node;
						node = node.parent;
						break;
				}
			}
			if (node != null)
				throw new Exception("Last node isn't closed");
			rootNode.name = "root";
			return rootNode;
		}
	}
}
