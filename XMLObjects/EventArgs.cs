using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLObjects
{
	public enum AccessType
	{
		GET,
		SET
	}

	public class EventArgs<T>
	{
		public bool canceled;
		public T value;
		public T Value => canceled ? default(T) : this.value;
		public XMLNode node;
		public readonly AccessType accessType;

		public EventArgs(XMLNode node, T value, AccessType accessType)
		{
			this.node = node; 
			this.value = value;
			this.accessType = accessType;
		}
	}

	public class EventArgs<T, A1> : EventArgs<T>
	{
		public A1 arg1;

		public EventArgs(XMLNode node, T value, A1 arg1, AccessType accessType) : base(node, value, accessType)
		{
			this.arg1 = arg1;
		}
	}
	public class EventArgs<T, A1, A2> : EventArgs<T>
	{
		public A1 arg1;
		public A2 arg2;

		public EventArgs(XMLNode node, T value, A1 arg1, A2 arg2, AccessType accessType) : base(node, value, accessType)
		{
			this.arg1 = arg1;
			this.arg2 = arg2;
		}
	}
	public class EventArgs<T, A1, A2, A3> : EventArgs<T>
	{
		public A1 arg1;
		public A2 arg2;
		public A3 arg3;

		public EventArgs(XMLNode node, T value, A1 arg1, A2 arg2, A3 arg3, AccessType accessType) : base(node, value, accessType)
		{
			this.arg1 = arg1;
			this.arg2 = arg2;
			this.arg3 = arg3;
		}
	}
}
